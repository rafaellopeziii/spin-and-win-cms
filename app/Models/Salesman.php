<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Salesman extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function customers(){
        
        return $this->hasMany('App\Models\Customer','dsp_id');
    }

    public function prizes(){
        return $this->hasMany('App\Models\Prize','dsp_code','dsp_code');
    }
}
