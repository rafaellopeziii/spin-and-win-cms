<?php

namespace App\Imports;

use App\Models\Prize;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class PrizesImport implements ToModel, WithHeadingRow
{
    use Importable;
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        if ($row['voucher_code']) {

            $prize = Prize::where('voucher_code', $row['voucher_code'])->first();

            if (!$prize) {

                $params = [
                    'prize_type' => $row['prize'],
                    'voucher_code' => $row['voucher_code']
                ];

                Prize::create($params);
            }
        }
    }
}
