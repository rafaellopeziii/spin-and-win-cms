<?php

namespace App\Imports;

use Carbon\Carbon;
use App\Models\Customer;
use App\Models\Salesman;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class SalesmanImport implements ToModel, WithHeadingRow, WithBatchInserts, WithChunkReading
{
    use Importable;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if(isset($row['dsp_code']))
        {
            //Instantiate
            $dspCode = $row['dsp_code'];
            $dspName = $row['dsp_name'];
            $distributorCode = $row['distributor_code'];
            $distributorName = $row['distributor_name'];

            $customerCode = $row['customer_code'];
            $customerName = $row['customer_name'];

            // $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@_";
            // $password = substr($row['dsp_code'], 4) . substr( str_shuffle( $chars ), 0, 8 ) ;
            $password = "cpsw21";

            $dsp = Salesman::where('dsp_code',$dspCode)->first();

            if(!$dsp)
            {
                $params =    [   
                    'distributor_code' => $distributorCode,
                    'distributor_name' => $distributorName,
                    'dsp_code'         => $dspCode, 
                    'dsp_name'         => $dspName,
                    'password'         => $password,    
                    
                ];
                 Salesman::create($params);
            }
           
            $dsp = Salesman::where('dsp_code',$dspCode)->first();
            $arr = [
                'dsp_id' => $dsp->id,
                'dsp_code' => $dspCode,
                'customer_code' => $customerCode,
                'customer_name' => $customerName,
            ];

           
            Customer::updateOrCreate(['customer_code' => $customerCode],$arr);
            
            
            
           

          
            
        }
    }
    public function batchSize(): int
    {
        return 100;
    }
    
    public function chunkSize(): int
    {
        return 100;
    }
}
