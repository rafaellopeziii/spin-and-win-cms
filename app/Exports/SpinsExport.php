<?php

namespace App\Exports;

use DB;
use Carbon\Carbon;
use App\Models\Spin;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class SpinsExport implements FromView, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    
    public function __construct($request)
    {
        $this->date =  Carbon::parse($request->date)->format('Y-m-d');
    }


    public function view(): View
    {
        return view('cms.table', [
            'customers' => $customers = DB::table('customers as c')
                ->rightJoin('spins as a', 'c.id', '=', 'a.customer_id')
                ->leftJoin('salesmen as u', 'c.dsp_code', '=', 'u.dsp_code')
                ->select(
                    'u.dsp_code',
                    'c.customer_code',
                    'c.customer_name',
                    'u.distributor_name',
                    'a.invoice',
                    'a.amount',
                    DB::raw('(SELECT COUNT(spin_count) FROM spins WHERE customer_id = a.customer_id 
                    AND batch = a.batch ) as spin'),
                    'c.phone',
                    DB::raw('DATE_FORMAT(a.date,"%c/%e/%Y") as date'),
                    DB::raw('TIME(a.date) as time'),
                    DB::raw('(SELECT prize FROM `spins` 
                          WHERE spin_count = 1 
                          AND customer_id = a.customer_id 
                          AND batch = a.batch
                          GROUP BY a.customer_code, a.batch ORDER BY spin_count ASC   LIMIT 1) as prize1'),
                    DB::raw('(SELECT prize FROM `spins` WHERE spin_count = 2 
                            AND customer_id = a.customer_id  
                            AND batch = a.batch
                            GROUP BY a.customer_code, a.batch ORDER BY spin_count ASC   LIMIT 1) as prize2'),
                    DB::raw('(SELECT prize FROM `spins` WHERE spin_count = 3 
                    AND customer_id = a.customer_id  
                    AND batch = a.batch
                    GROUP BY a.customer_code, a.batch ORDER BY spin_count ASC   LIMIT 1) as prize3')  
                )
                ->whereNotIn('u.distributor_code', ['CD01', 'PL01', 'CWD'])
                ->whereDate('a.created_at', $this->date)
                ->groupBy('a.customer_id')
                ->orderByRaw('DATE(a.date) DESC', 'TIME(a.date) DESC')
                ->get()
        ]);
    }

    public function headings(): array
    {
        return [
            'Salesman',
            'Customer_code',
            'Store Name',
            'Distributor',
            'Invoice',
            'Amount',
            'Good Sold',
            'Mobile Number',
            'Date',
            'Time',
            'Prize 1',
            'Prize 2',
            'Prize 3',

        ];
    }
}
