<?php

namespace App\Http\Controllers;

use App\Models\Salesman;
use Illuminate\Http\Request;
// use Maatwebsite\Excel\Facades\Excel;
use Excel;

class ExportExcelController extends Controller
{
    public function index()
    {
        $salesmen = Salesman::all();

        return view('cms.report', ['customers' => $salesmen]);
    }

    public function excel()
    {
        $salesmen = Salesman::all();
        // return $salesmen;
        $salesmen_array[] = array('ID','DSP Code', 'DSP Name');

        foreach($salesmen as $salesman)
        {

            // return $salesman;
            $salesmen_array[] = array(
                'ID' => $salesman->id,
                'DSP Code' => $salesman->dsp_code,
                'DSP Name' => $salesman->dsp_name
            );
        }

        Excel::create('Salesmen Data', function($excel) use($salesmen_array){
                $excel->setTitle('Salesmen Data');
                $excel->sheet('Salesmen Data', function($sheet) use($salesmen_array){
                    $sheet->fromArray($salesmen_array, null, 'A1',false,false);
                });
        })->download('xlsx');
    }
}
