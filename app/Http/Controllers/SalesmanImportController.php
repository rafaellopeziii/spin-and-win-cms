<?php

namespace App\Http\Controllers;

use App\Imports\SalesmanImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
class SalesmanImportController extends Controller
{
    public function show()
    {
        return view('salesman.import');
    }
    public function store(Request $request)
    {
        $file = $request->file('file')->store('imports');
        // Excel::import(new UsersImport, $file);
        (new SalesmanImport)->import($file);
        
        return back()->withStatus('Excel file imported successfully!');
    }
}
