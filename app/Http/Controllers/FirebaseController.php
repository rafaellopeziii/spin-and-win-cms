<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Kreait\Laravel\Firebase\Facades\Firebase;

class FirebaseController extends Controller
{
    
    public function index(){


        $database = Firebase::database();
        $reference = $database->getReference('Distributor/');
        $data = $reference->getValue();
        

        return $data;
        // return view('cms.report', ['data' => $data]);
        
       
    }

    public function show(){
        $database = Firebase::database();
        $reference = $database->getReference('Distributor/');
        
        $value = $reference->getValue();
        return $value;
    }
    public function create(){

    }

    public function store(){

    }
    public function edit(){

    }
    public function update(){

        $database = Firebase::database();
        $reference = $database->getReference('Distributor/');
        $child = 'CD01P00001';
        
         // Update Method
        $updates = [
            'customer_name' => 'Christian',
        ];
        $value = $reference->getChild($child)->getChild('customers/CWD00001');
        $value->update($updates);
    }
    
}
