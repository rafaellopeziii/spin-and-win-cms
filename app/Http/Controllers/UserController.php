<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Spin;
use App\Models\Prize;
use App\Models\Customer;
use App\Models\Salesman;
use App\Exports\SpinsExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Kreait\Laravel\Firebase\Facades\Firebase;
use App\Imports\SalesmanImport;
use App\Imports\PrizesImport;

use File;
use View;
use Response;

class UserController extends Controller
{

    public function index()
    {
        $prizeList = [];

        $prizes = Prize::select('prize_type')->distinct('')->get();
        

        foreach($prizes as $prize){
            
            $item = $prize->prize_type;
            $prizeCount = Prize::where('prize_type',$item)
                        ->where('prize_status', 1)
                        ->get()->count();
            $prizeList[$item] = [
                'count'=> $prizeCount,
                'type' => $item
            ];

        }

        return view('dashboard',['prizes' => $prizeList]);
    }

   
    public function showReports(Request $request)
    {
        // dd($request->date);
        $date = (isset($request->date)) ? Carbon::parse($request->date) : Carbon::now();

        $customers = DB::table('customers as c')
            ->rightJoin('spins as a', 'c.id', '=', 'a.customer_id')
            ->leftJoin('salesmen as u', 'c.dsp_code', '=', 'u.dsp_code')
            ->select(
                'a.batch',
                'u.dsp_code',
                'c.customer_code',
                'c.customer_name',
                'u.distributor_name',
                'a.invoice',
                'a.amount',
                DB::raw('(SELECT COUNT(spin_count) FROM spins WHERE customer_id = a.customer_id 
                AND batch = a.batch ) as spin'),
                'c.phone',
                DB::raw('DATE_FORMAT(a.date,"%c/%e/%Y") as date'),
                DB::raw('TIME(a.date) as time'),
                DB::raw('(SELECT prize FROM `spins` 
                          WHERE spin_count = 1 
                          AND customer_id = a.customer_id 
                          AND batch = a.batch
                          GROUP BY a.customer_code, a.batch ORDER BY spin_count ASC   LIMIT 1) as prize1'),
                DB::raw('(SELECT prize FROM `spins` WHERE spin_count = 2 
                          AND customer_id = a.customer_id  
                          AND batch = a.batch
                          GROUP BY a.customer_code, a.batch ORDER BY spin_count ASC   LIMIT 1) as prize2'),
                DB::raw('(SELECT prize FROM `spins` WHERE spin_count = 3 
                AND customer_id = a.customer_id  
                AND batch = a.batch
                GROUP BY a.customer_code, a.batch ORDER BY spin_count ASC   LIMIT 1) as prize3')       
            )
            ->whereNotIn('u.distributor_code', ['CD01', 'PL01', 'CWD'])
            ->whereDate('a.created_at', $date->format('Y-m-d'))
            ->groupBy('a.customer_id','a.batch')
            ->orderByRaw('DATE(a.date) DESC')
            ->orderByRaw('TIME(a.date) DESC')
            ->get();
            // ->paginate(5);
        // dd($customers);       
        return view('cms.report', ['customers' => $customers]);
        // return view('cms.report');
    }
    public function updateReports(Request $request)
    {
        if($request->date === "yesterday"){
            $dateSelected = Carbon::yesterday()->format('Y-m-d');
        }
        elseif($request->date === "now"){
            $dateSelected = Carbon::now()->format('Y-m-d');
        }

        
        
        try {
            $database = Firebase::database();
            $reference = $database->getReference('Distributor/');
            $data = $reference->getValue();

            // return response()->json($data);

            // Instantiate New Arrays
            $spinsForToday = [];
            $arr = [];
            $createSpin = [];
            $updateVoucherCode = [];

            //For Customers with signature

            $newCustomerRecord = [];
            $CustomerRecord = DB::table('customers')
                ->select('customer_code')
                ->where('signature', '!=', '0')
                ->get();
            $newCustomerRecord = array_column($CustomerRecord->toArray(), 'customer_code');

            //For Customers with spin records

            $customerSpins = Spin::All();
            $newSpinRecord = [];
            foreach ($customerSpins as $customerSpin) {
                $params = [
                    'customer_code' => $customerSpin->customer_code,
                    'voucher_code' => $customerSpin->voucher_code,
                    'date'        => $customerSpin->date
                ];
                array_push($newSpinRecord, $params);
            }


            foreach ($data as $dspCode => $dspValue) {
                $customers = (isset($dspValue['customers'])) ? $dspValue['customers'] : [];
                // return $customers;
                $customersForToday = [];
                $newCustomerValue = [];

                if (count($customers) > 0) {
                    foreach ($customers as $customerCode => $customerValue) {

                        $id = (isset($customerValue['customer_id'])) ? $customerValue['customer_id'] : null;
                        // $id = $customerValue['customer_id'];
                        // var_dump($id);
                        $customerInfo = (isset($customerValue['info'])) ? $customerValue['info'] : [];
                        $customerPrize = (isset($customerValue['Prize'])) ? $customerValue['Prize'] : [];

                        $prize = [];
                        $info = [];
                        $arr_prize = [];
                        $arr_date = [];
                        $arr_info = [];
                        $lastInfo = [];
                        $spinNum = 0;

                        foreach ($customerInfo as $infoKey => $infoValue) {
                            $info[$infoKey] = [
                                'date' => $infoValue['date'],
                                'invoice' => (isset($infoValue['invoice'])) ? $infoValue['invoice'] : null,
                                'phone' => $infoValue['phone'],
                                'spin' => (isset($infoValue['spin'])) ? $infoValue['spin'] : null,
                                'signatureBase64' => (isset($infoValue['signatureBase64'])) ? $infoValue['signatureBase64'] : null,
                                'amount' => (isset($infoValue['amount'])) ? $infoValue['amount'] : null,
                            ];
                            $spinNum = (isset($infoValue['spin'])) ? $infoValue['spin'] : null;
                        }

                        foreach ($customerPrize as $prizeKey => $prizeValue) {
                            // return $prizeValue;
                            $voucherCode = (isset($prizeValue['voucher_code'])) ? $prizeValue['voucher_code'] : null;
                            $prizeDate =  $prizeValue['date'];
                            $date = Carbon::parse($prizeDate)->format('Y-m-d');
                            

                            if ($date <= $dateSelected) {

                                // Count dates
                                array_push($arr_date, $date);
                                $dateCount = [];
                                $dateCount = array_count_values($arr_date);

                                if ($dateCount[$date] <= $spinNum) {
                                    $spin_record =
                                        count(
                                            array_filter(
                                                $newSpinRecord,
                                                function ($var) use ($customerCode, $voucherCode, $prizeDate) {
                                                    return ($var['customer_code'] == $customerCode
                                                        && $var['voucher_code'] == $voucherCode
                                                        && $var['date'] == $prizeDate);
                                                }
                                            )
                                        );

                                    if ($spin_record == 0) {
                                        if (isset($prizeValue['voucher_code'])) {
                                            $arr_prize[$prizeKey] = [
                                                'date' => $prizeValue['date'],
                                                'name' => $prizeValue['name'],
                                                'voucher_code' => $prizeValue['voucher_code']
                                            ];
                                        } else {
                                            $arr_prize[$prizeKey] = [
                                                'date' => $prizeValue['date'],
                                                'name' => $prizeValue['name']
                                            ];
                                        }

                                        //Customer's Prize
                                        $prize = $arr_prize;
                                    }
                                }
                            }
                        }

                        // var_dump($prize);
                        if (count($prize) > 0) {
                            $lastInfo[$infoKey] = end($info);
                            $arr_info = $lastInfo;

                            // UNSET SIGNATURE IF EXISTING
                            $signatureRecord = count(
                                array_filter(
                                    $newCustomerRecord,
                                    function ($var) use ($customerCode) {
                                        return ($var == $customerCode);
                                    }
                                )
                            );

                            if ($signatureRecord != 0) {
                                foreach ($arr_info as $infoKey => $infoValue) {
                                    unset($infoValue['signatureBase64']);
                                    $arr_info[$infoKey] = $infoValue;
                                }
                            }

                            //CREATE THE NEW ARRAY OF CUSTOMERS FILTERED DATA 
                            $newCustomerValue = [
                                'Prize' => $prize,
                                'customer_id' => $id,
                                'customer_code' => $customerCode,
                                'dsp_code' => $dspCode,
                                'info' => $arr_info
                            ];
                            $customersForToday[$customerCode] = $newCustomerValue;
                        }
                    }
                }

                // TOTAL OF CUSTOMERS SELECTED
                // if(count($customersForToday) > 0)
                // {
                //     array_push($arr,count($customersForToday));
                // }


                if (count($customersForToday) > 0) {

                    $spinsForToday[$dspCode] = [
                        'batch' => (isset($dspValue['batch'])) ? $dspValue['batch'] : 0,
                        'dsp_code' => $dspCode,
                        'customers' => $customersForToday
                    ];
                }
            }

            // var_dump(array_sum($arr));
            // return response()->json($spinsForToday,200);

            // get dsp code
            $updateCustomer = [];
            $updateCustomer2 = [];
            foreach ($spinsForToday as $dsp_key => $data_value) {

                $customers = (isset($data_value['customers'])) ? $data_value['customers'] : [];


                // get dsp customers
                foreach ($customers as $customerCode => $customer_value) {
                    $phone = '';
                    $signature = '';
                    $date = null;
                    $info = null;
                    $id = $customer_value['customer_id'];

                    //check if the customer has prize and info
                    $customer_info = (isset($customer_value['info'])) ? $customer_value['info'] : [];
                    $customer_prize = (isset($customer_value['Prize'])) ? $customer_value['Prize'] : [];
                    $spin_count = 1;

                    // get info
                    foreach ($customer_info as $info => $info_value) {
                        $date = $info_value['date'];
                        $phone = $info_value['phone'];
                        $signature = (isset($info_value['signatureBase64'])) ? $info_value['signatureBase64'] : null;
                        $invoice = (isset($info_value['invoice'])) ? $info_value['invoice'] : null;
                        $amount = $info_value['amount'];

                        if ($signature != null) {

                            // if($id == null){
                            //     array_push($updateCustomer2, array(  
                            //                                         'customer_code' => $customerCode,
                            //                                         'phone'       => $phone,
                            //                                         'signature'   => $signature,
                            //                                         'info_id'     => $info,
                            //                                         'info_date'   => $date,
                            //                                         'spin_status' => 0
                            //                                     )); 

                            // }
                            // else{
                            array_push($updateCustomer, array(
                                'id'          => $id,
                                'phone'       => $phone,
                                'signature'   => $signature,
                                'info_id'     => $info,
                                'info_date'   => $date,
                                'spin_status' => 0
                            ));
                            // }

                        } else {


                            // if($id == null){
                            //     array_push($updateCustomer2, array(  
                            //                                         'customer_code' => $customerCode,
                            //                                         'phone'       => $phone,
                            //                                         'info_id'     => $info,
                            //                                         'info_date'   => $date,
                            //                                         'spin_status' => 0
                            //                                     )); 

                            // }
                            // else{
                            array_push($updateCustomer, array(
                                'id'          => $id,
                                'phone'       => $phone,
                                'info_id'     => $info,
                                'info_date'   => $date,
                                'spin_status' => 0
                            ));
                            // }


                        }
                    }


                    // get prize 
                    foreach ($customer_prize as $prize_key => $prize_value) {

                        $voucherCode = (isset($prize_value['voucher_code'])) ? $prize_value['voucher_code'] : null;

                        $dsp_batch = $data_value['batch'];

                        array_push($createSpin, array(
                            'customer_id'   => $id,
                            'customer_code' => $customerCode,
                            'spin_count'    => $spin_count,
                            'prize'         => $prize_value['name'],
                            'voucher_code'  => $voucherCode,
                            'date'          => $prize_value['date'],
                            'batch'         => $dsp_batch,
                            'invoice'       => $invoice,
                            'prize_key'     => $prize_key,
                            'amount'      => $amount,
                            'created_at' =>  date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),
                        ));

                        if ($voucherCode != null) {
                            array_push($updateVoucherCode, $voucherCode);
                        }

                        $spin_count++;
                    }
                }
            }
            // var_dump($updateCustomer);
            $chunked = array_chunk($updateCustomer, 10, true);
            $customerInstance = new Customer;
            $index = 'id';
            foreach ($chunked as $chunk) {
                \Batch::update($customerInstance, $chunk, $index);
            }

            // if(count($updateCustomer2) > 0)
            // {
            //     $chunked2 = array_chunk($updateCustomer2,10, true);
            //     $customerInstance2 = new Customer;
            //     $index2 = 'customer_code';
            //     foreach($chunked2 as $chunk){
            //         \Batch::update($customerInstance2, $chunk, $index2);
            //     }
            // }
            Spin::insert($createSpin);
            Prize::whereIn('voucher_code', $updateVoucherCode)->update(['prize_status' => 0]);

            // return response()->json(['message' => 'Customers Spin Updated Successfully'], 200);

        } catch (Exception $exception) {
            Log::info($exception);
        }
        return redirect()->route('reports');
    }

    public function resetSpins(Request $request)
    {
        $batch = $request->batch;
        $users = Salesman::with('customers')
            ->whereNotIn('distributor_code', ['CWD'])
            ->get();

        $newUsers = [];

        foreach ($users as $user) {
            $newCustomers = [];

            $customersNum = count($user->customers);
            if ($customersNum > 0) {
                foreach ($user->customers as $customer) {

                    if ($customer->signature != '0') {

                        $spin_status = Spin::where(['batch' => $batch, 'customer_id' => $customer->id])
                        ->first();

                        if (isset($spin_status)) {

                            $newCustomers[$customer->customer_code] =
                                [
                                    'customer_id' => $customer->id,
                                    'customer_code' => $customer->customer_code,
                                    'customer_name' => $customer->customer_name,
                                    'dsp_code'      => $customer->dsp_code,
                                    'spin_status'   => 0,
                                    'info'          =>  [
                                        $customer->info_id => [
                                            'date' => $customer->info_date,
                                            'phone' => $customer->phone,
                                            'signatureBase64' => 'R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=='
                                        ]
                                    ]
                                ];
                        } else {
                        $newCustomers[$customer->customer_code] =
                            [
                                'customer_id' => $customer->id,
                                'customer_code' => $customer->customer_code,
                                'customer_name' => $customer->customer_name,
                                'dsp_code'      => $customer->dsp_code,
                                'spin_status'   => 1,
                                'info'          =>  [
                                    $customer->info_id => [
                                        'date' => $customer->info_date,
                                        'phone' => $customer->phone,
                                        'signatureBase64' => 'R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=='
                                    ]
                                ]
                            ];
                        }


                        // return $infoParams;
                    } else {
                        $newCustomers[$customer->customer_code] =
                            [
                                'customer_id' => $customer->id,
                                'customer_code' => $customer->customer_code,
                                'customer_name' => $customer->customer_name,
                                'dsp_code'      => $customer->dsp_code,
                                'spin_status'   => 1

                            ];
                    }
                }

                unset($user->customers);
                // $newUsers[$user->dsp_code] = $user;
                $newUsers[$user->dsp_code] = [
                    'batch' => $batch,
                    'dsp_code' => $user->dsp_code,
                    'distributor_name' => $user->distributor_name,
                    'status' => $user->status,
                    'password_fb' => $user->password,
                    'customers' => $newCustomers
                ];
            }
            // $newUsers[$user->dsp_code]['customers'] = $newCustomers;
        }
       
        
        $test['data'] = json_encode($newUsers);
        $fileName = $batch. 'batch.json';
        File::put(public_path('/upload/json/' . $fileName), $test);
        return Response::download(public_path('upload/json/' . $fileName));
        
    }

    public function export(Request $request)
    {
        $date = Carbon::parse($request->date)->format('F d, Y');
        // dd($request);
        // return $this->excel->download(new SpinsExport,'spins.xlsx');
        return Excel::download(new SpinsExport($request), 'Palmolive Spin and Win Winners of ' . $date . '.xlsx');
    }

    public function showImportUsers()
    {


        return view('salesman.import');
    }
    public function importUsers(Request $request)
    {
        $file = $request->file('file')->store('imports');
        // Excel::import(new UsersImport, $file);
        (new SalesmanImport)->import($file);

        return back()->withStatus('Excel file imported successfully!');
    }

    public function showImportPrizes()
    {
        $prizeList = [];

        $prizes = Prize::select('prize_type')->distinct('')->get();
        

        foreach($prizes as $prize){
            
            $item = $prize->prize_type;
            $prizeCount = Prize::where('prize_type',$item)
                        ->where('prize_status', 1)
                        ->get()->count();
            $prizeList[$item] = [
                'count'=> $prizeCount,
                'type' => $item
            ];

        }

        return view('prize.import',['prizes' => $prizeList]);

        return view('prize.import');
    }
    public function importPrizes(Request $request)
    {
        $file = $request->file('file')->store('imports');
        // Excel::import(new UsersImport, $file);
        (new PrizesImport)->import($file);

        return back()->withStatus('Excel file imported successfully!');
    }

    public function exportPrizes(){


        $users  =  Salesman::with('prizes')
                            ->whereNotIn('distributor_code', ['PL01', 'CD01', 'CWD'])
                            ->get();
        $newUsers = [];

        foreach ($users as $user) {

            $newPrizes = [];
            foreach ($user->prizes as $prize) {
                if ($prize['prize_status'] == 1) {
                    $newPrizes[$prize->prize_type][$prize->voucher_code] = $prize;
                }
            }
            unset($user->prizes);
            $newUsers[$user->dsp_code] = $newPrizes;
        }

        
        $test['data'] = json_encode($newUsers);

        $fileName = 'Prize.json';

        File::put(public_path('/upload/json/' . $fileName), $test);

        return Response::download(public_path('upload/json/' . $fileName));

    }

    public function resetPrizes(){

        //Set dsp_code to null
        Prize::where('prize_status', 1 )
             ->update(['dsp_code' => null]);

        $prizes = Prize::select('prize_type')->distinct('')->get();
        foreach($prizes as $prize){
            // var_dump($prize->prize_type);
            $prizeType = $prize->prize_type;
            $users = Salesman::whereNotIn('distributor_code', ['PL01', 'CD01', 'CWD'])->get();
            $availableItem = Prize::where(['prize_type' => $prizeType, 'dsp_code' => null])->get();

        // Compute possible number of prizes for each dsp

        $usersNum = $users->count();
        $availableItemCount =  $availableItem->count();

        // return([$usersNum,$availableItemCount]);

        if ($availableItemCount > 0) {

            $result = $availableItemCount / $usersNum;
            $prizeCount = (int) round($result);
            // return([$usersNum,$availableItemCount,$prizeCount]);
            $availableItemPerUser = ($prizeCount * $usersNum);
            // return([$availableItemPerUser,$availableItemCount]);


            foreach ($users as $user) {

                $totalItem = Prize::where([
                    'prize_type' => $prizeType,
                    'dsp_code' => null
                ])->get()->count();

                //get total user's vouchercode
                $params = [
                    'dsp_code' => $user->dsp_code,
                    'prize_type' => $prizeType,
                    'prize_status' => 1
                ];
                $userItem = Prize::where($params)->get()->count();

                if ($userItem < $prizeCount) {
                    if ($totalItem != 0) {
                        for ($x = 0; $x < $prizeCount; $x++) {
                            $newPrize = Prize::where([
                                'prize_type' => $prizeType,
                                'dsp_code' => null
                            ])->first();

                            if ($newPrize) {
                                $newPrize->update(['dsp_code' => $user->dsp_code]);
                            }
                        }
                    }
                }
            }

            if ($availableItemPerUser < $availableItemCount) {
                // return true;
                $prizeCount += 1;
                foreach ($users as $user) {
                    $totalItem =  Prize::where([
                        'prize_type' => $prizeType,
                        'dsp_code' => null
                    ])->get()->count();

                    //get total user's CJ vouchercode
                    $Chicken = [
                        'dsp_code' => $user->dsp_code,
                        'prize_type' => $prizeType,
                        'prize_status' => 1
                    ];
                    $userItem = Prize::where($Chicken)->get()->count();

                    if ($userItem < $prizeCount) {

                        if ($totalItem > 0) {

                            for ($x = 0; $x < 1; $x++) {

                                $newPrize = Prize::where([
                                    'prize_type' => $prizeType,
                                    'dsp_code'   => null
                                ])->first();

                                if ($newPrize) {

                                    $newPrize->update(['dsp_code' => $user->dsp_code]);
                                }
                            }
                        }
                    }
                }
            }
        }
        }
        
        $users  =  Salesman::with('prizes')
                            ->whereNotIn('distributor_code', ['PL01', 'CD01', 'CWD'])
                            ->get();
        $newUsers = [];

        foreach ($users as $user) {

            $newPrizes = [];
            foreach ($user->prizes as $prize) {
                if ($prize['prize_status'] == 1) {
                    $newPrizes[$prize->prize_type][$prize->voucher_code] = $prize;
                }
            }
            unset($user->prizes);
            $newUsers[$user->dsp_code] = $newPrizes;
        }

        
        $test['data'] = json_encode($newUsers);

        $fileName = 'Prize.json';

        File::put(public_path('/upload/json/' . $fileName), $test);

        return Response::download(public_path('upload/json/' . $fileName));
    }
}
