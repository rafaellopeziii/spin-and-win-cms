@extends('layouts.app')

@section('content')


<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col">
           <div class=""><h3>Spin And Win Daily Reports</h3></div>
        </div>
        
    </div>
    <div class="row mt-2 ">
      <div class="col-md-6 ">
        <form class="form-inline" action="{{route('reports')}}" method="get">
          {{ csrf_field()}}
          <div class="form-group mx-sm-3 mb-2">
            <label for="date" class="mx-3 mb-2">Synced Date</label>
            <input class="form-control" type="date" name="date" id="date" value="{{ request()->input('date')  }}" required>
          </div>
          <button class="btn btn-primary text-white mb-2" type="submit">
            <svg class="c-icon">
            <use  xlink:href="{{ asset('vendors/coreui/icons/svg/free.svg#cil-reload')}}"></use>
            </svg>
          </button>
        </form>
      </div>
      <div class="col-md-6 ">
        <div class="row justify-content-end">
            {{-- <div class="col-md-3">
              <a href="{{route('report.update')}}" class="btn btn-primary text-white">Sync Report</a>
            </div>
            <div class="col-md-3">
              <button type="button" class="btn btn-primary text-white" data-toggle="modal" data-target="#excelModal">
                  Excel
              </button>
            </div> --}}
            
            <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
              {{-- <div class="btn-group mr-2" role="group" aria-label="First group">
                <a href="{{route('report.update')}}" class="btn btn-primary text-white">Sync Report</a>
              </div> --}}
              <div class="btn-group mr-2" role="group" aria-label="First group">
                <button type="button" class="btn btn-primary text-white" data-toggle="modal" data-target="#excelModal">
                  Excel
                </button>
              </div>
              <div class="btn-group mr-2" role="group" aria-label="First group">
                <button type="button" class="btn btn-primary text-white" data-toggle="modal" data-target="#syncModal">
                  Sync Report
                </button>
              </div>
            </div>
        </div>
       
      </div>
    </div>
    @if (count($customers) > 0)
    <div class="row justify-content-center mt-2">
    
    <div class="table-responsive-xl rounded overflow-auto">
      
      <table class="table table-light">
        <thead>
            <tr>
            <th>Batch</th>
            <th>Salesman </th>
            <th>Customer Code</th>
            <th scope="col">Store Name</th>
            <th scope="col">Distributor</th>
            <th scope="col">Invoice</th>
            <th scope="col">Amount</th>
            <th scope="col">Good Sold</th>
            <th scope="col">Mobile Number</th>
            <th scope="col">Date</th>
            <th scope="col">Time</th>
            <th scope="col">Prize 1</th>
            <th scope="col">Prize 2</th>
            <th scope="col">Prize 3</th>
            </tr>
        </thead>
        <tbody>
         @foreach($customers as $row)
             <tr>
               <td>{{$row->batch}}</td>
                <td>{{$row->dsp_code}}</td>
                <td>{{$row->customer_code}}</td>
                <td>{{$row->customer_name}}</td>
                <td>{{$row->distributor_name}}</td>
                <td>{{$row->invoice}}</td>
                <td>{{$row->amount}}</td>
                <td>{{$row->spin}}</td>
                <td>{{$row->phone}}</td>
                <td>{{$row->date}}</td>
                <td>{{$row->time}}</td>
                <td>{{$row->prize1 ? $row->prize1 : 'N/A' }}</td>
                <td>{{$row->prize2 ? $row->prize2 : 'N/A' }}</td> 
                <td>{{$row->prize3 ? $row->prize3 : 'N/A' }}</td> 
             </tr>    
        @endforeach 
        
        </tbody>
    </table>
   
    </div>
    
    {{-- {{ $customers->links() }} --}}
    
  </div>
  @else
    <div class="row justify-content-center">
      <h1>There is no record</h1>
    </div>
    @endif
</div>
<!-- Modal -->
<div class="modal fade" id="excelModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="excelModalLabel">Spin and Win Daily Report</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{route('excel')}}" method="POST">
                {{ csrf_field()}}
                <div class="form-group row">
                  <label class="col-2 col-form-label">Date</label>
                  <div class="col-10">
                    <input class="form-control" type="date" name="date" id="date" required>
                  </div>
                </div>
                
                <button class="btn btn-success col-12" type="submit">Export Excel</button>
                
              </form>
        </div>
        {{-- <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div> --}}
      </div>
    </div>
  </div>

  <div class="modal fade" id="syncModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="excelModalLabel">Sync Report</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{route('report.update')}}" method="GET">
                {{ csrf_field()}}
                <select name="date" class="form-select form-select-lg mb-3 col-12" >
                  
                  <option value="now">Winners from Today</option>
                  <option selected value="yesterday">Winners from Yesterday</option>
                </select>
                
                <button class="btn btn-success col-12" type="submit">Sync</button>
                
              </form>
        </div>
        
      </div>
    </div>
  </div>

@endsection