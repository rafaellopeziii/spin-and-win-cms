<table class="table-sm table-responsive table-bordered">
    <thead>
        <tr>
        <th>Salesman </th>
        <th>Customer Code</th>
        <th scope="col">Store Name</th>
        <th scope="col">Distributor</th>
        <th scope="col">Invoice</th>
        <th scope="col">Amount</th>
        <th scope="col">Good Sold</th>
        <th scope="col">Mobile Number</th>
        <th scope="col">Date</th>
        <th scope="col">Time</th>
        <th scope="col">Prize 1</th>
        <th scope="col">Prize 2</th>
        <th scope="col">Prize 3</th>
        </tr>
    </thead>
    @foreach($customers as $row)
         <tr>
            <th>{{$row->dsp_code}}</th>
            <td>{{$row->customer_code}}</td>
            <td>{{$row->customer_name}}</td>
            <td>{{$row->distributor_name}}</td>
            <td>{{$row->invoice}}</td>
            <td>{{$row->amount}}</td>
            <td>{{$row->spin}}</td>
            <td>{{$row->phone}}</td>
            <td>{{$row->date}}</td>
            <td>{{$row->time}}</td>
            <td>{{$row->prize1 ? $row->prize1 : '\N'}}</td>
            <td>{{$row->prize2 ? $row->prize2 : '\N'}}</td>
            <td>{{$row->prize3 ? $row->prize3 : '\N'}}</td>
         </tr>   
    @endforeach
</table>