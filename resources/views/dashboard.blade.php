@extends('layouts.app')

@section('content')
    <div class="container">
        {{-- <div class="row justify-content-center">
            <h2>Prizes</h2>
        </div> --}}
        <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h3>Remaining Prizes</h3>
                    </div>
                        
                    <div class="card-body">
                        @foreach ($prizes as $prize => $value )
                            <h5>{{$prize}} : {{$value['count']}}</h5>   
                        @endforeach
                    </div>
                </div>
            </div>
           
        </div>
    </div>
@endsection