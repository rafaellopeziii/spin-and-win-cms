@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header"><h3>Import Salesmen and Customers</h3></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div>
                        <h5>Header's Format</h5>
                        <ul>
                            <li>distributor_code</li>
                            <li>distributor_name</li>
                            <li>customer_code</li>
                            <li>customer_name</li>
                            <li>dsp_code</li>
                            <li>dsp_name</li>
                        </ul>
                    </div>    
                    <form action="/salesman/import" method="POST" enctype="multipart/form-data" >
                        @csrf
                        .<div class="form-group">
                          
                          <input type="file" name="file" required>
                          <button type="submit" class="btn btn-primary">Import</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header"><h3>Spin Status Reset</h3></div>

                <div class="card-body">
                  <form action={{route('spin.reset')}} method="get">
                    @csrf
                    <div class="form-group">
                        <label class="form-label" for="typeNumber">Batch</label>
                        <input type="number" name="batch"id="typeNumber" class="form-control" required />
                    </div>
                    <button type="submit" class="btn btn-primary">Reset</button>
                 </form>  
                     
                </div>
            </div>
        </div>
    </div>
</div>
@endsection