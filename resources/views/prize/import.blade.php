@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h3>Import Prizes</h3>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form action="/prize/import" method="POST" enctype="multipart/form-data">
                        @csrf
                        .<div class="form-group">
                          
                          <input type="file" name="file" required>
                          <button type="submit" class="btn btn-primary">Import</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h3>Reset Salesman Prize's List</h3>
                </div>
                    
                <div class="card-body">
                    <p>Reminder: Please sync the data from firebase before reset the prizes.  </p>
                    <a href={{route('resetPrizes')}} class="btn btn-primary">Reset salesman's prizes</a>
                </div>

               
            </div>
        </div>
        
        
    </div>
    <div class="row justify-content-center">
        
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h3>Remaining Prizes</h3>
                </div>
                    
                <div class="card-body">
                    @foreach ($prizes as $prize => $value )
                        <h5>{{$prize}} : {{$value['count']}}</h5>   
                    @endforeach
                </div>

            </div>
        </div>
    </div>
</div>
@endsection