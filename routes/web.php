<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/





Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    // define your route, route groups here
    Route::get('/firebase', 'App\Http\Controllers\FirebaseController@index');
    Route::get('/', [App\Http\Controllers\UserController::class, 'showReports'])->name('dashboard');

    //Users Controller
    Route::get('/reset-spins', [App\Http\Controllers\UserController::class, 'resetSpins'])->name('spin.reset');
    Route::get('/reports/update', [App\Http\Controllers\UserController::class, 'updateReports'])->name('report.update');
    Route::get('/reports', [App\Http\Controllers\UserController::class, 'showReports'])->name('reports');
    Route::post('/reports/export', [App\Http\Controllers\UserController::class, 'export'])->name('excel');
    Route::get('/salesman/import', [App\Http\Controllers\UserController::class, 'showImportUsers'])->name('import.users');
    Route::post('/salesman/import', [App\Http\Controllers\UserController::class, 'importUsers']);
    Route::get('/prize', [App\Http\Controllers\UserController::class, 'showImportPrizes'])->name('import.prizes');
    Route::post('/prize/import', [App\Http\Controllers\UserController::class, 'importPrizes']);
    Route::get('/reset-prizes', [App\Http\Controllers\UserController::class, 'resetPrizes'])->name('resetPrizes');
    Route::get('/export-prizes', [App\Http\Controllers\UserController::class, 'exportPrizes']);
    
});
