<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('dsp_id');
            $table->string('dsp_code');
            $table->string('customer_code');
            $table->string('customer_name');
            $table->boolean('spin_status')->default(true);
            $table->string('phone')->default('0');
            $table->longText('signature')->default('0');
            $table->string('info_id')->nullable();
            $table->dateTime('info_date')->nullable();
            $table->boolean('active')->default(true);

            $table->timestamps();

            $table->foreign('dsp_id')->references('id')->on('salesmen');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
